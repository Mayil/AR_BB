﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Sounds
{
    public string name;
    public AudioClip clip;
    [Range(0.0f, 1.0f)]
    public float volume = 0.5f;
    [Range(0.5f, 1.5f)]
    public float pitch = 1f;

    AudioSource source;


    public void SetSource(AudioSource _source)
    {
        source = _source;
        source.clip = clip;
    }

    public void Play()
    {
        source.volume = volume;
        source.pitch = pitch;
        source.Play();
    }

    public void Stop()
    {
        source.Stop();
    }
}


public class AudioManager : MonoBehaviour {
    public static AudioManager instance;


    [SerializeField]
    Sounds[] sounds;


    void Awake()
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            GameObject _go = new GameObject("Sound_" + i + "_" + sounds[i].name);
            _go.transform.SetParent(this.transform);
            sounds[i].SetSource(_go.AddComponent<AudioSource>());

        }
        if (instance != null)
        {
            Debug.Log("More than 1 AudioManager instance");
        }
        else
        {
            instance = this;
        }

    }

    public void PlaySound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Play();
                Debug.Log("Sound is Played");
                return;
            }
        }

        //_name not found
        Debug.LogWarning("AudioManager:Sound not found" + _name);

    }

    public void StopSound(string _name)
    {
        for (int i = 0; i < sounds.Length; i++)
        {
            if (sounds[i].name == _name)
            {
                sounds[i].Stop();
                Debug.Log("Sound is Stopped");
                return;
            }
        }

        //_name not found
        Debug.LogWarning("AudioManager:Sound not found" + _name);

    }

}
