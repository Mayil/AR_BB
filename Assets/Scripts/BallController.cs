﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BallController : MonoBehaviour {

    
    Rigidbody ball;
    Vector3 BallPosition;
    //Vector2 touchOrigin = -Vector2.one;
    //float horizontal = 0;
    //float vertical = 0,x,y;

    public Text Score;
    public GameObject effect;

    float score;    
    float InitialTime, FinalTime;
    Vector2 IntTouch, FinalTouch;
    private float Xaxis, Yaxis, Zaxis;
    Vector3 RequireForce;

    public GameManager GameMan;
    public AudioManager audioMan;

    void Start () {
        ball = GetComponent<Rigidbody>();
        BallPosition = GetComponent<Transform>().transform.position;
        ball.useGravity=false;
        GameMan = GameManager.instance;
        audioMan = AudioManager.instance;
        effect.SetActive(false);    
    }
	
    void Update()
    {
        Score.text = "Score:" + score.ToString();
    }
    public void OnTouchDown()
    {
        //InitialTime = Time.time;
        IntTouch = Input.mousePosition;
    }

    public void OnTouchUp()
    {
        //FinalTime = Time.time;
        FinalTouch = Input.mousePosition;
        BallThrow();
    }

    private void BallThrow()
    {
        audioMan.PlaySound("Throw");
        Xaxis = FinalTouch.x - IntTouch.x;
        Yaxis = FinalTouch.y - IntTouch.y;
       // Zaxis = FinalTime - InitialTime;
        ball.useGravity = true;
        RequireForce = new Vector3(Xaxis*0.3f, Yaxis, Yaxis*0.5f);
        ball.velocity = RequireForce*0.01f;
        
    }
	
	/*void FixedUpdate () {
        
		if(Input.touchCount>0)
        {
            Debug.Log("Detected");
            Touch myTouch = Input.touches[0];
            if(myTouch.phase==TouchPhase.Began)
            {
                touchOrigin = myTouch.position;
            }
            else if(myTouch.phase==TouchPhase.Ended && touchOrigin.x>0)
            {
                Vector2 touchEnd = myTouch.position;
                x = (touchEnd.x - touchOrigin.x)%3;
                y = (touchEnd.y - touchOrigin.y)%2;
                touchOrigin.x = -1;
                touchOrigin.y = -1;
                horizontal = x;
                vertical = y;
            }
            /* if(x>0 || y>0)
             {
                 horizontal = x > 0 ? 1:-1;
                 vertical = y > 0 ? 1 : -1;
             }*/
            
             //  ball.AddForce(new Vector3(0, vertical + 4f, horizontal+2.0f) * 0.1f, ForceMode.Impulse);
        //}

       
	//}

    public void Reload()
    {
        SceneManager.LoadScene("Game");
    }

     void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Score")
        {
            audioMan.PlaySound("Score");
            effect.SetActive(true);
            Debug.Log("Trigerred");
            score += 2;
        }
        if(other.tag=="Ground")
        {
            audioMan.PlaySound("Ground");
            Debug.Log("Ground");
            gameObject.transform.position = BallPosition;
            ball.useGravity = false;
            ball.velocity = new Vector3(0, 0, 0);
            effect.SetActive(false);
        }
            
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag=="Ring")
        {
            audioMan.PlaySound("Ring");
        }
        if(other.gameObject.tag=="Board")
        {
            audioMan.PlaySound("Board");
        }
    }

}
