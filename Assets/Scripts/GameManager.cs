﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager instance;
    [SerializeField]
    GameObject[] Exitobjs;
    
    public Animator exitAnim;

    public AudioManager audioMan;
	void Start () {
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        audioMan = AudioManager.instance;
        audioMan.PlaySound("BGM");
        Exitobjs = GameObject.FindGameObjectsWithTag("ExitObj");
        hideExit();
      
    }
	

	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            
            showExit();
        }

    }

    private void showExit()
    {
        foreach(GameObject g in Exitobjs)
        {
            g.SetActive(true);
        }
    }

    private void hideExit()
    {
        foreach (GameObject g in Exitobjs)
        {
            g.SetActive(false);
        }
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Cancel()
    {        
        StartCoroutine("AnimationPlay");  
    }

    IEnumerator AnimationPlay()
    {
        exitAnim.SetTrigger("Cancel");
        yield return new WaitForSeconds(1f);
        hideExit();
        //StopCoroutine("AnimationPlay");
    }
}
